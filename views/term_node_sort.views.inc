<?php

/**
 * Implements hook_views_data_alter().
 */
function term_node_sort_views_data_alter(&$data) {
  if (isset($data['term_node']['vid'])) {
    $data['term_node']['vid']['sort'] = array(
      'title' => t('Node revision'),
      'help' => t('Sorting by the node revision rather than Node: Created can produce more efficient queries on node views filtered by term ID.'),
      'handler' => 'views_handler_sort',
    );
  }
}

